provider "aws" {
    version = "~> 2.0"
    region  = var.gov_zones[0]
    access_key = "xxx"
    secret_key = "xxx"
}

data "aws_availability_zones" "aza" {
  state="available"
}


# Create a VPC shared_services
resource "aws_vpc" "shared_services" {
  cidr_block = var.cidr_block  
   tags = {
    Name = "Shared Services"    
  }
}

resource "aws_internet_gateway" "gateway" {
  vpc_id= aws_vpc.shared_services.id

}
resource "aws_route_table" "web-public-rt" {
  vpc_id= aws_vpc.shared_services.id

  route{
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.gateway.id
  }
}

resource "aws_route_table_association" "web-public-rt" {
  subnet_id=aws_subnet.subnet-shared-resources.id
  route_table_id = aws_route_table.web-public-rt.id
}

resource "aws_security_group" "shared" {
  name = "shared_vpc_sec"
  description ="Allow incoming HTTP connections and SSH access"

  ingress{
    from_port=80
    to_port=80
    protocol="tcp"
    cidr_blocks =["0.0.0.0/0"]
  }
  ingress{
    from_port=8080
    to_port=8080
    protocol="tcp"
    cidr_blocks =["0.0.0.0/0"]
  }

  ingress{
    from_port=443
    to_port=443
    protocol="tcp"
    cidr_blocks =["0.0.0.0/0"]
  }
  ingress{
    from_port=-1
    to_port=-1
    protocol="icmp"
    cidr_blocks =["0.0.0.0/0"]
  }
  ingress{
    from_port=22
    to_port=22
    protocol="tcp"
    cidr_blocks =["0.0.0.0/0"]
  }
  egress{
    from_port=0
    to_port=-0
    protocol=-1
    cidr_blocks =["0.0.0.0/0"]
  }
  vpc_id=aws_vpc.shared_services.id

}



resource "aws_subnet" "subnet-shared-resources" {
  vpc_id     = aws_vpc.shared_services.id
  availability_zone = var.gov_zones[1]
  map_public_ip_on_launch = true
  cidr_block = "10.10.20.0/24"  
 
  tags = {
    Name = "Shared resources subnet A"
  }
}

resource "aws_instance" "shared-server" {
  ami= var.amis[0]
  security_groups = [aws_security_group.shared.id]
  subnet_id = aws_subnet.subnet-shared-resources.id
  key_name="test_jenkins"
  instance_type = "t2.micro"

  tags = {
    Name = "Shared Server"
  }
}

















//Security VPC
resource "aws_vpc" "security" {
  cidr_block = var.cidr_block
  instance_tenancy = "dedicated"
   tags = {
    Name = "Security"
  }
}

resource "aws_vpc" "dev-data-west" {
  cidr_block = var.cidr_block
  instance_tenancy = "dedicated"
   tags = {
    Name = "dev-data-west"
  }
}


resource "aws_vpc" "dev-k8s-west" {
  cidr_block = var.cidr_block
  instance_tenancy = "dedicated"
   tags = {
    Name = "dev-k8s-west"
  }
}


resource "aws_subnet" "subnet-a" {
  vpc_id     = aws_vpc.dev-k8s-west.id
  availability_zone = var.gov_zones[1]
  map_public_ip_on_launch = true
  cidr_block = "10.10.20.0/24"  
 
  tags = {
    Name = "K8 Public Subnet A"
  }
}
resource "aws_subnet" "subnet-b" {
  vpc_id     = aws_vpc.dev-k8s-west.id
  availability_zone = var.gov_zones[2]
  map_public_ip_on_launch = true
  cidr_block = "10.10.21.0/24"  

  tags = {
    Name = "K8 Public Subnet B"
  }
}

output "k8_out" {
  value=aws_vpc.dev-k8s-west
}

output "data_out" {
  value=data.aws_availability_zones.aza
}




