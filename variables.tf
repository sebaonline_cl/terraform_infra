
variable "cidr_block" {
  default= "10.10.0.0/16"
}

variable "gov_zones" {
    default=[
        "us-west-2",
        "us-west-2a",
        "us-west-2b",
        "us-gov-west-1",
        "us-gov-west-1a",
        "us-gov-west-1b"
    ]
}
variable "amis" {
    default=[
        "ami-002932067f4b7675e" // jenkins on ubuntu
    ]
  
}


